/*
 * this is a fake validator meant to represent the real validation process
 * this is only for testing
 * the validator performs the following functions
 * 1. mintNewContract
 * 	creates a new hashrate contract
 * 2. getListOfContracts
 * 	returns a list of contracts for the user to use
 * 3. Run a contract
 * 	simulates the validation process and returns a pre-defined true/false value after
 * 	a pre-programmed amount of time
 * 4. change contract state
 * 	changes the state of a hashrate contract
*/


//initialize web3 connection
let Web3 = require('web3');
let web3 = new Web3('http://127.0.0.1:8545');


//initialize fs for importing json info
let fs = require('fs');


//import webfacing smart contract JSON code
let webFacingRaw = fs.readFileSync('contracts/WebFacing.json');
let webFacingContract = JSON.parse(webFacingRaw);


//import hashrate smart contract JSON code
let hashrateRaw = fs.readFileSync('contracts/Implementation.json'); //interface may change to another name
let hashrateContract = JSON.parse(hashrateRaw);


//get the deployed webfacing contracts address
let deployedWebFacingAddress = webFacingContract.networks['5777']['address']; 


//create a contract obejct which can have its methods called
let webFacing = new web3.eth.Contract(webFacingContract.abi, deployedWebFacingAddress);

console.log(webFacing.methods)

//get list of addresses and reserve index 0 as validator
let accounts;
web3.eth.getAccounts().then(r => accounts = r);


let changeState = async (hashrateContractAddress, desiredState) => { //function that can change the state of a contract. Will make a web3 call
	//expecting a true/false to be returned from setChangeContractState
	let contract = new web3.eth.Contract(hashrateContract.abi, hashrateContractAddress);
	let statechange = await contract
													.methods
													.setChangeState(
													desiredState).
													send({from: accounts[5]}).
													on('receipt', (r) => console.log('changed state')).
													on('error', (e) => console.log(`error when chaning state: ${e}`));
}

//mints a new hashrate contract
let mintNewContract = async (cost, hashrate) => {
	await webFacing.methods.setCreateRentalContract(
		cost, hashrate, accounts[1]).send({from: accounts[5], gas: 1000000}).
		on('receipt', (r) => console.log('made a new contract')).
		on('error', (e) => console.log(`error when making new contract: ${e}`));
}


//gets a list of all the hashrate contracts
let getHashrateContracts = async() => {
	//let contracts = await webFacing.methods.getListOfContracts().send({from: accounts[0]})
	let contracts;
	await webFacing.methods.getListOfContracts().call().then(r => contracts = r);
	console.log(contracts)
	return contracts;
}


let sleep = async (ms) => { //sleep function to simulate the timeline of a contract
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}


//initialize express server
const express = require('express')
const app = express()
const port = 7545

app.get('/', (req, res) => {
	res.send('Hello World')
})


app.get('/mint/:cost/:hashrate/:validator', async (req, res) =>{
	//function to mint a new hashrate contract (will be required for testing purposes)
	let cost = Number(req.params.cost);
	let hashrate = Number(req.params.hashrate);
	console.log('im gonna mint the police')
	await mintNewContract(cost, hashrate);
	res.json({address: address});
})


app.get('/current_contracts', async (req, res) => {
	let contracts = await getHashrateContracts(); //change call
	res.json(contracts)
})

//takes in 2 variables, time: int, pass: bool
//will mark a contract as done if pass is true, will mark
//a contract as invalid if pass is fail
app.get('/validate/:time/:pass', async (req, res) => { //validation api call
	let time = req.params.time //delay time
	let pass = req.params.pass.toLowerCase() //decide if it passes
	await sleep(Number(time));
	res.json({result: pass})
	//wait for a set amount of time and then return a result
})


//changes the status of the passed in hashrate contract
//the desired state is the state which the contact can be running
//the address is the specific smart contract address which
//the state change is targeting
app.get('/changeState/:desired_state/:address', async (req, res) => {
	let desired_state = req.params.desired_state.toLowerCase() //ending state of contract
	let address = req.params.address
	switch(desired_state) {
		case 'active': 
			await changeState(address,'active');
			break;
		case 'available':
			await changeState(address,'available');
			break;
		case 'running':
			await changeState(address,'running');
			break;
		case 'complete':
			await changeState(address,'complete');
			break;
		default:
			console.log('not a valid state')
			break;
	}
	res.json({result: 'changed'})
})


app.get('/completionStatus/:address/:delay', async(req, res) => {
	let num = Math.random()*100;
	res.json({hashes_done:num})

})

app.listen(port, () => {
	console.log(`listening on ${port}`)
})

